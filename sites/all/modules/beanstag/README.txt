INTRODUCTION
------------

This module allows to set page titles, meta descriptions and meta tags
according to url/path alias.

INSTALLATION
------------

1. Place the BeansTag module directory in your modules folder (this will
   usually be "sites/all/modules/").

2. Enable the module.

3. Add/Edit/Delete any BeansTag entries Structure » Manage BeansTag.

4. Go to Configuration » Seach and metadata » Manage BeansTag » 
   BeansTag settings to configure the BeansTag behaviour.

OTHER RESOURCES
---------------
For more information, visit http://eureka.ykyuen.info/tag/beanstag/

AUTHOR
------
Yuen Ying Kit - http://drupal.org/user/1006470

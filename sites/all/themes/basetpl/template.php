<?php

/**
 * @file
 * template.php
 */
function basetpl_preprocess_page(&$variables, $hook) {
	$search_box = drupal_render(drupal_get_form('search_form'));
	$variables['search_box'] = $search_box;
	
	$variables['menu_global'] = menu_navigation_links('menu-global');
	// $variables['menu_footer_menu'] = menu_navigation_links('menu-footer-menu');
	// $variables['menu_custom_links'] = menu_navigation_links('menu-custom-links');

	$variables['contain'] = false;
	$nodetype = array('t108','hp701', 't905', 't904', 't704', 't703', 't702', 't100', 'blog', 't103', 'industry_partners', 't205', 'page', 'sectioned_faq');
	if (isset($variables['node'])) {
		$check = $variables['node']->type;
		if($check != (in_array($check, $nodetype))) {
			$variables['contain'] = true;
		}
	}
}
function basetpl_preprocess_node(&$variables) {
  
    $variables['submitted'] = t('!username', 
	  array('!username' => $variables['name'], '!datetime' => $variables['date'])
	);

}

function basetpl_preprocess_html(&$variables) {
   drupal_add_css('http://fonts.googleapis.com/css?family=Raleway:400,500,700', array('group' => CSS_THEME));
}
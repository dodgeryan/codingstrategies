(function($){
  $(".dropdown-toggle").addClass('disabled');
  $('.navbar ul.nav li.dropdown').hover(function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn();
  }, function() {
    $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
  });
// MORE +
  
  $('a[href^="#"]').on('click', function (event) {
      event.preventDefault();
   } );
  $('.anchor-link').on('click', function (event) {
      event.preventDefault();
  });

// PAGE TABS
  $('#page-tabs').tab();
  $('#page-tabs a[href="#tab1"]').tab('show');
  $('#page-tabs .nav-tabs li a').click(function (e) {
    e.preventDefault();
    $(this).tab('show');
  });




// PANELS
  $('.panel-default').on('show.bs.collapse', function () {
    $(this).addClass('active');
    
  });

  $('.panel-default').on('hide.bs.collapse', function () {
    $(this).removeClass('active');
    
  });

})(jQuery);
(function($){

// PAGE TABS
    $('#page-tabs').tab();
    $('#page-tabs a[href="#tab1"]').tab('show');
    $('#page-tabs a').click(function (e) {
      e.preventDefault();
      $(this).tab('show');
    });

  $(window).load(function(){
  //LOGO CAROUSEL RESPONSIVE

      $("#pres-logo-list").carouFredSel({
        width: "100%",
        height: 65,
        auto: false,
        items: {
          width: "variable",
          height: 65,
          visible     : {
            min         : 1,
            max         : 4
          }
        },
        scroll: 1,      
        prev: "#prev3",
        next: "#next3",
        swipe: true,
        mousewheel: true,
        swipe: {
          onMouse: true,
          onTouch: true
        }
      }).parent().css("margin", "auto");    
});
    $(window).load(function(){



// ANIMATION SKROLLR

    var s = skrollr.init({
      render: function(data) {
        //Log the current scroll position.
        //console.log(data.curTop);
      }
    });

    // offset center to compensate for line width
    var width = $('.vert-line').width() / 2;
    $('.vert-line').css('margin-left',-width);

    var resizeTimer;

    function nagOnResize() {
        //pause for 1/4 of a second to see if the window stopped moving
        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(doNagOnResize, 250);
    }

    function doNagOnResize() {
        $(window).unbind('resize', nagOnResize); //pause the event
        var h = $(window).height(), w = $(window).width();
        if(w > 768) {
            //smaller than iPad Vertical - turn skrollr off
            //var s = skrollr.destroy();
        }
        $(window).resize(nagOnResize); //continue the event
    };

    $(window).resize(nagOnResize);
 

  });

})(jQuery);

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <style type="text/css">
  <?php 
   if(!empty($content['field_preview_image']['#items'])) {
    $imgbg = $content['field_preview_image']['#items'][0]['uri'];
    $bgimg = file_create_url($imgbg); 

    print '
    .blog-feature-image { height: 493px; position: relative;}
    #blog-feature-image { 
      background: url('.$bgimg.') no-repeat center center scroll; 
      position: absolute;
      overflow: auto;
      width: 100%;
      height: 100%;
      -webkit-background-size: cover;
      -moz-background-size: cover;
      -o-background-size: cover;
      background-size: cover;
      filter: progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$bgimg.'", sizingMethod="scale");
      -ms-filter: "progid:DXImageTransform.Microsoft.AlphaImageLoader(src="'.$bgimg.'", sizingMethod="scale")";}
      ';
    }?>
    
  </style>
  <section class="blog-feature-image row">
    <div id="blog-feature-image"></div>
  </section>
  <section id="blog-content">
    <div class="container">
      
      <?php if((empty($content['field_preview_image']['#items'])) && (!empty($content['field_blog_main']['#items']))) { ?>        
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <?php 
              print theme(
                'image_style', 
                array(
                  'style_name'  => 'default', 
                  'path'        => $node->field_blog_main['und'][0]['uri'], 
                  'attributes'  =>array(
                    'alt'       => $node->field_blog_main['und'][0]['alt'],
                    'width'     => '100%',
                    'height'    => '400'
                  )
                )
              ); 
            ?>
          </div>
        </div>
      <?php } ?>
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <div class="row">
            <h1 id="blog-title" class="col-xs-12"><?php print $title; ?></h1>
          </div>
          <div id="blog-info" class="blog-info row">
            <div class="col-xs-12 col-sm-5"><span class="fa fa-user"></span> By <?php print $submitted ?> on <?php print date('F jS, Y', $node->created); ?></div>
            <?php if(!empty($node->field_blog_category['und'])) { ?>
              <div class="hidden-xs col-sm-7"><span class="fa fa-tag"></span> <a href="/taxonomy/term/<?php print render($node->field_blog_category['und'][0]['taxonomy_term']->tid); ?>"><?php print render($node->field_blog_category['und'][0]['taxonomy_term']->name); ?></a></div>
            <?php } ?>
          </div>
          <div class="row">
            <?php if(!empty($content['field_podcast'])) { ?>
              <div class="blog-mp3 col-xs-12">
                <?php print render($content['field_podcast']); ?>
              </div>
            <?php } ?>
          </div>
          <div class="row">            
            <div class="blog-text col-xs-12">
              <?php print render($content['body']); ?>
            </div>            

          </div>
          <section id="blog-social" class="section">           
            <div class="row">
              <div class="col-xs-12 col-sm-6 col-sm-push-6">
                <?php
                  $blog_title = urlencode($title);
                  $blog_url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                  $short_body = substr($content['body']['#items'][0]['value'], 0, 200);
                  $blog_summary = urlencode($short_body); 
                ?>
                <p class="social-title"><span class="share-title">Share this post:</span> &nbsp;
                <a class="fa-stack fa-lg blog-social-link twitter-share" href="http://twitter.com/intent/tweet?url=<?php print $blog_url; ?>&text=<?php print $blog_title; ?>" target="_blank">
                  <span class="fa fa-circle fa-stack-2x sky-blue"></span>
                  <span class="fa fa-twitter fa-stack-1x fa-inverse"></span>
                </a>
                <a class="fa-stack fa-lg blog-social-link linkedin-share" href="http://www.linkedin.com/shareArticle?mini=true&url=<?php print $blog_url; ?>&title=<?php print $blog_title; ?>&summary=<?php print $blog_summary; ?>&source=Dodge%20Communications" target="_blank">
                  <span class="fa fa-circle fa-stack-2x navy-blue"></span>
                  <span class="fa fa-linkedin fa-stack-1x fa-inverse"></span>
                </a>
                <a class="fa-stack fa-lg blog-social-link email-share" href="mailto:?subject=<?php print $title; ?>&amp;body=Check out this article:  <?php print $blog_url; ?>">
                  <span class="fa fa-circle fa-stack-2x orange"></span>
                  <span class="fa fa-envelope fa-stack-1x fa-inverse"></span>
                </a>
                </p>
              </div>
              <!-- <div class="col-xs-12 col-sm-6 col-sm-pull-6 comment-trigger"> -->
                <!-- <button class="btn btn-comments" type="button" data-toggle="collapse" data-target="#blog-comments"><span class="fa fa-comment"></span>  Comments</button> -->
              <!-- </div> -->
            </div>
            <!-- <div id="blog-comments" class="collapse row blog-comments"> -->
              <?php // print render($content['comments']); ?>
            <!-- </div>            -->
          </section>
        </div>
      </div>
    </div> <!-- /container -->
  </section>
  
  
        <?php
          // $view = views_get_view('blog');
          // $view->set_display('block_4');
          // $output = $view->preview();
           // var_dump($view);
          // now print the view.
          // if ($view->result) { ?>
            <!-- <section id="related-blogs">
              <div class="container section">
                <div class="col-xs-12"><h2 class="related-blogs-title">Related Posts</h2></div>
                <div class="row"> -->
              
                  <?php //print views_embed_view('blog', $display_id = 'block_4'); ?>
                <!-- </div>
              </div>
            </section> -->
         <?php //} ?>

<?php if(!empty($content['field_pardot_form']['#items'])) { ?>
<section id="pardot-cta">
  <div class="container">
    <div class="row pardot-cta">
      <?php if($content['field_pardot_form']['#items'][0]['summary']){ ?>
      
        <div class="panel-group cta-button-dropdown col-xs-12 col-sm-10 col-sm-offset-1" id="accordion">
          <div class="panel panel-default">
            
            <button class="panel-heading btn-primary btn-block btn-collapse" style="white-space:normal;" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><?php print render($content['field_pardot_form']['#items'][0]['summary']); ?></button>
             
             <div id="collapseOne" class="panel-collapse collapse">

              <div class="panel-body">
                    <?php print render($content['field_pardot_form']['#items'][0]['value']); ?>
              </div>
            </div>
          </div>
        </div>
      <?php  } else { ?>
        <section id="pardot-form" class="section">
          <div class="pardot-form row">
            <div class="col-xs-12 col-sm-10 col-sm-offset-1">
              <?php print render($content['field_pardot_form']['#items'][0]['value']); ?>
            </div>
          </div>
        </section>
      <?php } ?>
    </div>
  </div>
</section> 
<?php } ?>
</article>
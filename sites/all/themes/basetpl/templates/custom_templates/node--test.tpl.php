<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>

    <section id="page-text" class="section">
      <div class="container">
        <div class="row">          
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 page-text">
            
            <?php if (!empty($content['body']['#items'])) { ?>
              <div class="lead"><?php print render($content['body']['#items'][0]['value']); ?></div>
            <?php } ?>
          </div>
        </div>
      </div>
    </section>
  <section id="content-cards" class="section">
    <div class="container">
      <div class="row">
        <?php 
          // $cols = 0;
          // if ((!empty($content['field_client_cs'])) && (!empty($content['field_client_cs_2'])) && (!empty($content['field_client_cs_3']))) {
          //   $cols = 3;
          // } if ((!empty($content['field_client_cs'])) && (!empty($content['field_client_cs_2'])) && (empty($content['field_client_cs_3']))) {
          //   $cols = 2;
          // } if ((!empty($content['field_client_cs'])) && (empty($content['field_client_cs_2'])) && (!empty($content['field_client_cs_3']))) {
          //   $cols = 1;
          // } 
        ?>
         
          <?php // FIRST COLUMN RENDER 
            //if(!empty($content['field_client_cs']['#items'])) {
              $col1count=0;
              // echo '<pre>';
              // var_dump($content['field_client_cs_2']); 
              // echo '</pre>';
              // switch ($cols) {
              //   case '1':
              //     print '<div class="col-sm-12">';
              //     break;
              //   case '2':
              //     print '<div class="col-sm-12 col-md-6">';
              //     break;
              //   default:
              //     print '<div class="col-sm-12 col-md-4">';
              //     break;
              // }          
              foreach($content['field_logo_set']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col1count++;
                  

                              print '<div class="card-logo">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_partner_logo['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'client-logo',
                                    'alt'       => $field_col_obj->field_partner_logo['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            
                          
                
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            //} // .endif 
          ?>
          

        
      </div> <!-- /. row -->
    </div> <!-- /. container -->
  </section>

</article> <!-- /.node -->

<script type="text/javascript">
// (function($){

//   var $myIntro = $(this).find('.intro-expanded');
//   var $card = $('.card-content');


//    // Collapse event trigger
//   $card.on('click', function() {
//     $card.not($(this)).find('.collapse.in').collapse('hide'); // close all other cards
//     $(this).find('.intro-expanded').collapse('show'); // fire event
    
//     $card.unbind('click', '.intro-expanded');

    
//   });
//   $('.expand').on('click', function(e){
//     $(this).parents('.card-intro').find('.intro-expanded').collapse('toggle'); // fire event
//     $(this).removeClass('expanded');  
//     e.preventDefault();
//   })
  
   
//   $('.intro-expanded').on('show.bs.collapse', function () { // Change button text on reveal
//     $(this).parent().find('.expand-section .expand span.expand-default').hide();    
//     $(this).parent().find('.expand-section .expand span.expand-collapse').show();

//     $("html, body").animate({ scrollTop: $(this).parent().find('.expand').offset().top -375 }, 600);
//     $(this).parents('.card-content').addClass('card-expanded').removeClass('opened'); // add visited styling
    
//   });

//   $('.intro-expanded').on('hide.bs.collapse', function () { // Restore button text on close
//     $(this).parent().find('.expand-section .expand span.expand-collapse').hide();
//     $(this).parent().find('.expand-section .expand span.expand-default').show();
//     $(this).parents('.card-content').addClass('opened').removeClass('card-expanded');
    

//   });

  
// })(jQuery)


</script>
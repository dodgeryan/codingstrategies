<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>

  
<!-- INTRO -->
<section id="overview" class="section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <?php if(!empty($content['body']['#items'])); { 
          print render($content['body']['#items'][0]['value']); 
        } ?>
      </div>
    </div>
  </div>
</section>

<!-- SECTION ONE -->
<?php if(!empty($content['field_cs_objectives']['#items'])) { ?>
  <section id="objectives" class="section">
    <div class="container">
      <div class="row">
        <div class="col-sm-7">
          <h3 class="section-title"><?php print render($content['field_section_title']['#items'][0]['value']); ?></h3>
          <?php 
          foreach($content['field_cs_objectives']['#items'] as $entity_uri) {
            $field_col_item = entity_load('field_collection_item', $entity_uri);
            foreach ($field_col_item as $field_col_obj) { 
            ?>
              <div class="section-set">
                <div class="row">
                  <?php 
                    if(!empty($field_col_obj->field_cs_section_img)) {
                      if(!empty($field_col_obj->field_cs_section_img['und'][0]['field_file_image_alt_text']['und'])) {
                        $imgalt = $field_col_obj->field_cs_section_img['und'][0]['field_file_image_alt_text']['und'][0]['value'];
                      } else {
                        $imgalt = '';
                      }
                      
                      print '<div class="col-xs-1 section-icon">';
                        print theme(
                          'image_style', 
                          array(
                            'style_name'  => 'default', 
                            'path'        => $field_col_obj->field_cs_section_img['und'][0]['uri'], 
                            'alt'         => $imgalt
                          )
                        );      
                      print '</div>';
                    }
                  ?>
                  <?php 
                    if(!empty($field_col_obj->field_cs_section_txt)) {
                      print '<div class="col-xs-11 section-text">';
                        print render($field_col_obj->field_cs_section_txt['und'][0]['value']);
                      print '</div>';
                    } 
                  ?>
                </div>
              </div>            
            <?php
            } 
          }?>
        </div>
        <!--  RESOURCES SECTION -->
        <?php if(!empty($content['field_cd_resources']['#items'])) { ?>
          <div class="col-sm-4 col-sm-offset-1">
            <?php 
              foreach($content['field_cd_resources']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);
                foreach ($field_col_item as $field_col_obj) { 
                ?>
                  <div class="row">
                    
                    <section id="testimonials" class="hidden-xs">
                      <?php if (!empty($field_col_obj->field_section_content['und'])) { 
                        $quote = count($field_col_obj->field_section_content['und']);
                        for ($i=0; $i < $quote ; $i++) { ?>                  
                          <div class="testimonial">
                            <div class="testimonial-content">
                              <i class="fa fa-quote-left"></i><span class="testimonial-title">testimonal</span>
                              <?php print render($field_col_obj->field_section_content['und'][$i]['value']); ?>
                            </div>
                          </div>  
                        <?php } ?>                        
                      <?php } ?>
                    </section>

                    <section class="resources-list hidden-xs">
                      <h4 class="section-title"><?php print render($field_col_obj->field_cs_resources_title['und'][0]['value']); ?></h4>
                      <?php 
                        if(!empty($field_col_obj->field_cs_resources_pdf['und'])) {
                          print '<div id="pdf-list">';
                            print '<ul class="pdf-list">';
                              $lengthPDF = count($field_col_obj->field_cs_resources_pdf['und']);
                              for ($i=0; $i < $lengthPDF ; $i++) { 
                                $uri = $field_col_obj->field_cs_resources_pdf['und'][$i]['uri'];  // file path as uri: 'public://';
                                $pdf_name = $field_col_obj->field_cs_resources_pdf['und'][$i]['filename'];
                                $pdf_path = file_create_url($uri); 
                                print '<li><a class="pdf-list-item" href="';
                                  print $pdf_path;
                                print '" target="_blank">';
                                  print $pdf_name;
                                print '</a></li>';
                              }
                            print '</ul>';
                          print '</div>';
                        } 
                      ?>
                    </section>
                  </div>            
                <?php
                } 
              }
            ?>
          </div>
        <?php } ?>
      </div>
    </div>
   </section>
<?php } ?>

<!-- BASIC TEXT -->
<?php if(!empty($content['field_basic_text']['#items'])) { ?>
<section id="basic-text" class="section">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        <?php print render($content['field_basic_text']['#items'][0]['value']); ?>
      </div>
    </div>
  </div>
</section>
<?php } ?>

<!-- CTA SECTION -->
<?php if(!empty($content['field_cta']['#items']) || !empty($content['field_cta_link'])) { ?>
  <section class="page-cta section">    
    <div class="container">
      <div class="row">
        <?php if(!empty($content['field_cta']['#items'])) { ?>
          <div class="cta-text col-xs-12 col-sm-8 col-sm-offset-2">          
            <?php print render($content['field_cta']['#items'][0]['value']); ?>
          </div>  
        <?php } ?> 
        <?php if(!empty($content['field_cta_link'])) { ?>               
          <div class="col-xs-12 cta-link">
            <?php 
              print '<a class="cta-btn btn btn-default" href="';
                print render($content['field_cta_link']['#items'][0]['url']);
              print '">';
                print render($content['field_cta_link']['#items'][0]['title']);
              print '</a>';
            ?>         
          </div>
        <?php } ?>
      </div>
    </div>
   
  </section>
<?php } ?>
  <!-- END  CTA -->  
</article>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

  
    <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>
 
  
  <?php if(!empty($content['field_hp701_page_section'])) { // MAIN CONTENT SECTIONS
    
    $count = 0;
    $row = true;
    
    foreach($content['field_hp701_page_section']['#items'] as $entity_uri) {
      $field_col_item = entity_load('field_collection_item', $entity_uri);
      
      foreach ($field_col_item as $field_col_obj) {
        $count++; ?>
        <section id="section-<?php print $count ?>" class="hp701-section <?php print (($row = !$row)? 'odd' : 'even'); ?>">
          <div class="container child">
            <div class="row">
              <?php if(!empty($field_col_obj->field_hp701_summary_img['und'])) { ?>
              <div class="col-xs-12 col-sm-5 summary-image text-center <?php print (($row = !$row)? '' : 'offset1'); ?> ">
                <?php print theme(
                  'image_style', 
                  array(
                    'style_name'  => 'default', 
                    'path'        => $field_col_obj->field_hp701_summary_img['und'][0]['uri'], 
                    'alt'         => $field_col_obj->field_hp701_summary_img['und'][0]['field_file_image_alt_text']['und'][0]['value']
                  )
                ); ?>          
                </div>
              <?php } ?>
              <div class="col-xs-12 col-sm-7 <?php print (($row = !$row)? '' : 'offset1'); ?>">
                <h2><?php print render($field_col_obj->field_hp701_section_title['und'][0]['value']);
                if(!empty($field_col_obj->field_hp701_section_subtitle['und'][0])) { 
                  print '<br /><em class="sub-heading">';
                  print render($field_col_obj->field_hp701_section_subtitle['und'][0]['value']);
                  print "</em>";
                } ?></h2>
                <?php 
                print render($field_col_obj->field_hp701_summary_text['und'][0]['value']);
                if (!empty($field_col_obj->field_hp701_summary_cta['und'][0])) {
                  print '<div class="cta"><a class="cta-link" href="' .render($field_col_obj->field_hp701_summary_cta['und'][0]['url']).'">';
                    print render($field_col_obj->field_hp701_summary_cta['und'][0]['title']);
                  print '</a>';
                }
                
                ?>
              </div>
            </div>
          </div> <!-- container -->
        </section> <!-- /.node -->
      <?php } /* foreach */
    } /* foreach */
  } /* if */ ?>
  
  <?php if(!empty($content['field_hp701_section'])) { // TRIPLE SECTIONS
    $count = 0;
      
    foreach($content['field_hp701_section']['#items'] as $entity_uri) {
      $field_col_item = entity_load('field_collection_item', $entity_uri);
      $count++;
        // first 3 summaries
      foreach ($field_col_item as $field_col_obj) {
        print '<section id="overview" class="section ';
        print (($row = !$row)? 'odd' : 'even');
        print '"><div class="container"><div class="row">';  

          $length = count($field_col_obj->field_hp701_front_summary['und']);
          for($i=0; $i<$length;$i++){
            print '<div class="col-xs-12 col-sm-4 mobile">';
              print render($field_col_obj->field_hp701_front_summary['und'][$i]['value']);
            print '</div>';
          }
        print '</div></div></section>';
      }
    }
  } ?>
  

  <?php if(!empty($content['field_hp701_logos'])) { // FEATURED PARTNERS CAROUSEL ?>
  
    <section id="clients" class="section">
      <div class="container">
        <div class="list_carousel responsive">
          <ul id="hp701-logo-list">
          <?php $length = count($content['field_hp701_logos']['#items']);
            for($i=0; $i<$length;$i++){ ?>
            <li>
              <?php print
              theme('image_style', 
                array(
                  'style_name' => 'default', 
                  'path' => $node->field_hp701_logos['und'][$i]['uri'], 
                  'alt' => $node->field_hp701_logos['und'][$i]['field_file_image_alt_text']['und'][0]['value']
                )
              ) ?>
            </li>
            <?php } ?>
          </ul>
          <div class="clearfix"></div>
          <a id="prev3" class="prev carousel-control" href="#">‹</a><a id="next3" class="next carousel-control" href="#">›</a>
        </div>
      </div>
    </section>
  <?php } ?>
</article> <!-- /.node -->

<script type="text/javascript" src="/sites/all/themes/basetpl/js/jquery.carouFredSel-6.2.1-packed.js"></script>
<script type="text/javascript">
  (function($){
    // HP701 CAROUSEL
    $(window).load(function() {
      $("#hp701-logo-list").carouFredSel({
        width: "100%",
        height: 125,
        auto: false,
        items: {
          width: 'variable',
          height: 125,
          visible     : {
            min         : 1,
            max         : 5
          }
        },
        scroll: 1,      
        prev: "#prev3",
        next: "#next3",
        swipe: true,
        mousewheel: true,
        swipe: {
          onMouse: true,
          onTouch: true
        }
      }).parent().css("margin", "auto");
    });



  })(jQuery);


</script>

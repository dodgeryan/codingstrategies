<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>

    <section id="page-text" class="section">
      <div class="container">
        <div class="row">          
          <div class="col-xs-12 col-sm-10 col-sm-offset-1 page-text">
            
            <?php if (!empty($content['body']['#items'])) { ?>
              <div class="lead"><?php print render($content['body']['#items'][0]['value']); ?></div>
            <?php } ?>
          </div>
        </div>
      </div>
    </section>
  <section id="content-cards" class="section">
    <div class="container">
      <div class="row">

        <?php 
          $cols = 0;
          if ((!empty($content['field_logo_set_1'])) && (!empty($content['field_logo_set_2'])) && (!empty($content['field_logo_set_3']))) {
            $cols = 3;
          } if ((!empty($content['field_logo_set_1'])) && (!empty($content['field_logo_set_2'])) && (empty($content['field_logo_set_3']))) {
            $cols = 2;
          } if ((!empty($content['field_logo_set_1'])) && (empty($content['field_logo_set_2'])) && (!empty($content['field_logo_set_3']))) {
            $cols = 1;
          } 
        ?>
         
          <?php // FIRST COLUMN RENDER 
            if(!empty($content['field_logo_set_1']['#items'])) {
              $col1count=0;
              
              switch ($cols) {
                case '1':
                  print '<div class="col-sm-12">';
                  break;
                case '2':
                  print '<div class="col-sm-12 col-md-6">';
                  break;
                default:
                  print '<div class="col-sm-12 col-md-4">';
                  break;
              }          
              foreach($content['field_logo_set_1']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col1count++;
                  

                  ?>
                  
                  <div class="row card-item">
                    <div class="card-content row <?php if(empty($field_col_obj->field_pardot_form['und'])) {?>no-expand<?php } ?>">
                        <div class="card-intro">
                          <?php  
                            
                            // CARD LOGO 
                            if (!empty($field_col_obj->field_partner_logo['und'])) {
                              print '<div class="card-logo">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_partner_logo['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'client-logo',
                                    'alt'       => $field_col_obj->field_partner_logo['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            }
                          ?>
                        
                        <!--  INTRO SECTION -->
                        
                          
                          
                          <!-- COLLAPSED SECTION -->
                          <div id="#collapse-<?php print $col1count; ?>" class="collapse fade intro-expanded">
                            <?php if(!empty($field_col_obj->field_pardot_form['und'])) { ?>
                              <div class="cs-text">
                                <?php print render($field_col_obj->field_pardot_form['und'][0]['summary']); ?>
                              </div>
                            <?php } ?>
                            
                            <!-- PARDOT FORM -->
                            <?php if(!empty($field_col_obj->field_pardot_form['und'][0]['value'])){ ?>
                              <div class="cs-pardot">
                                <?php print render($field_col_obj->field_pardot_form['und'][0]['value']); ?>      
                              </div>
                            <?php } ?>  
                                   
                          </div> <!-- //. COLLAPSE SECTION -->
                          <!-- EXPAND BUTTON -->
                          <?php if(!empty($field_col_obj->field_pardot_form['und'])) { ?>
                          <div class="expand-section">
                            <a class="expand" data-toggle="collapse"  data-target="#collapse-<?php print $col1count; ?>" href="#collapse-<?php print $col1count; ?>">
                              <span class="expand-default"><?php print render($field_col_obj->field_cta_line['und'][0]['value']); ?> &nbsp; <span class="fa fa-plus-circle"></span></span>
                              <span class="expand-collapse">Collapse  &nbsp; <span class="fa fa-minus-circle"></span></span>
                            </a>
                          </div>
                          <?php } ?>
                        </div> <!-- //. INTRO -->
                      
                    </div> <!-- //. CARD CONTENT -->
                  </div> <!-- //. CARD CELL -->

                <?php
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            } // .endif 
          ?>
          <?php // SECOND COLUMN RENDER 
            if(!empty($content['field_logo_set_2']['#items'])) {
              $col2count=0;
              switch ($cols) {
                case '1':
                  print '<div class="col-sm-12">';
                  break;
                case '2':
                  print '<div class="col-sm-12 col-md-6">';
                  break;
                default:
                  print '<div class="col-sm-12 col-md-4">';
                  break;
              }           
              foreach($content['field_logo_set_2']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col2count++;

                  ?>
                  <div class="row card-item">
                    <div class="card-content row <?php if(empty($field_col_obj->field_pardot_form['und'])) {?>no-expand<?php } ?>">
                        <div class="card-intro">
                          <?php  
                            
                            // CARD LOGO 
                            if (!empty($field_col_obj->field_partner_logo['und'])) {
                              print '<div class="card-logo">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_partner_logo['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'client-logo',
                                    'alt'       => $field_col_obj->field_partner_logo['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            }
                          ?>
                        
                        <!--  INTRO SECTION -->
                        
                          
                          
                          <!-- COLLAPSED SECTION -->
                          <div id="#collapse-<?php print $col2count; ?>" class="collapse fade intro-expanded">
                            <?php if(!empty($field_col_obj->field_pardot_form['und'])) { ?>
                              <div class="cs-text">
                                <?php print render($field_col_obj->field_pardot_form['und'][0]['summary']); ?>
                              </div>
                            <?php } ?>
                            
                            <!-- PARDOT FORM -->
                            <?php if(!empty($field_col_obj->field_pardot_form['und'][0]['value'])){ ?>
                              <div class="cs-pardot">
                                <?php print render($field_col_obj->field_pardot_form['und'][0]['value']); ?>      
                              </div>
                            <?php } ?>  
                                                                               
                          </div> <!-- //. COLLAPSE SECTION -->
                          <!-- EXPAND BUTTON -->
                          <?php if(!empty($field_col_obj->field_pardot_form['und'])) { ?>
                          <div class="expand-section">
                            <a class="expand" data-toggle="collapse"  data-target="#collapse-<?php print $col2count; ?>" href="#collapse-<?php print $col2count; ?>">
                              <span class="expand-default"><?php print render($field_col_obj->field_cta_line['und'][0]['value']); ?> &nbsp; <span class="fa fa-plus-circle"></span></span>
                              <span class="expand-collapse">Collapse  &nbsp; <span class="fa fa-minus-circle"></span></span>
                            </a>
                          </div>
                          <?php } ?>
                        </div> <!-- //. INTRO -->
                      
                    </div> <!-- //. CARD CONTENT -->
                  </div> <!-- //. CARD CELL -->

                <?php
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            } // .endif 
          ?>
          <?php // THIRD COLUMN RENDER 
            if(!empty($content['field_logo_set_3']['#items'])) {
              $col3count=0;
              switch ($cols) {
                case '1':
                  print '<div class="col-sm-12">';
                  break;
                case '2':
                  print '<div class="col-sm-12 col-md-6">';
                  break;
                default:
                  print '<div class="col-sm-12 col-md-4">';
                  break;
              }           
              foreach($content['field_logo_set_3']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col3count++;

                  ?>
                  <div class="row card-item">
                    <div class="card-content row <?php if(empty($field_col_obj->field_pardot_form['und'])) {?>no-expand<?php } ?>">
                        <div class="card-intro">
                          <?php  
                            
                            // CARD LOGO 
                            if (!empty($field_col_obj->field_partner_logo['und'])) {
                              print '<div class="card-logo">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_partner_logo['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'client-logo',
                                    'alt'       => $field_col_obj->field_partner_logo['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            }
                          ?>
                        
                        <!--  INTRO SECTION -->
                        
                          
                          
                          <!-- COLLAPSED SECTION -->
                          <div id="#collapse-<?php print $col3count; ?>" class="collapse fade intro-expanded">
                            <?php if(!empty($field_col_obj->field_pardot_form['und'])) { ?>
                              <div class="cs-text">
                                <?php print render($field_col_obj->field_pardot_form['und'][0]['summary']); ?>
                              </div>
                            <?php } ?>
                            
                            <!-- PARDOT FORM -->
                            <?php if(!empty($field_col_obj->field_pardot_form['und'][0]['value'])){ ?>
                              <div class="cs-pardot">
                                <?php print render($field_col_obj->field_pardot_form['und'][0]['value']); ?>      
                              </div>
                            <?php } ?>  
                                                                               
                          </div> <!-- //. COLLAPSE SECTION -->
                          <!-- EXPAND BUTTON -->
                          <?php if(!empty($field_col_obj->field_pardot_form['und'])) { ?>
                          <div class="expand-section">
                            <a class="expand" data-toggle="collapse"  data-target="#collapse-<?php print $col3count; ?>" href="#collapse-<?php print $col3count; ?>">
                              <span class="expand-default"><?php print render($field_col_obj->field_cta_line['und'][0]['value']); ?> &nbsp; <span class="fa fa-plus-circle"></span></span>
                              <span class="expand-collapse">Collapse  &nbsp; <span class="fa fa-minus-circle"></span></span>
                            </a>
                          </div>
                          <?php } ?>
                        </div> <!-- //. INTRO -->
                      
                    </div> <!-- //. CARD CONTENT -->
                  </div> <!-- //. CARD CELL -->

                <?php
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            } // .endif 
          ?>
        
      </div> <!-- /. row -->
    </div> <!-- /. container -->
  </section>

</article> <!-- /.node -->

<script type="text/javascript">
(function($){

  var $myIntro = $(this).find('.intro-expanded');
  var $card = $('.card-content');


   // Collapse event trigger
  $card.on('click', function() {
    $card.not($(this)).find('.collapse.in').collapse('hide'); // close all other cards
    $(this).find('.intro-expanded').collapse('show'); // fire event
    
    $card.unbind('click', '.intro-expanded');

    
  });
  $('.expand').on('click', function(e){
    $(this).parents('.card-intro').find('.intro-expanded').collapse('toggle'); // fire event
    $(this).removeClass('expanded');  
    e.preventDefault();
  })
  $('.ie8 .card-expanded .expand').on('click', function(e){
    $(this).parents('.card-intro').find('.intro-expanded').removeClass('in');
    e.preventDefault();
    console.log('ie event')
  })
  
   
  $('.intro-expanded').on('show.bs.collapse', function () { // Change button text on reveal
    $(this).parent().find('.expand-section .expand span.expand-default').hide();    
    $(this).parent().find('.expand-section .expand span.expand-collapse').show();

    $("html, body").animate({ scrollTop: $(this).parent().find('.expand').offset().top -375 }, 600);
    $(this).parents('.card-content').addClass('card-expanded').removeClass('opened'); // add visited styling
    
  });

  $('.intro-expanded').on('hide.bs.collapse', function () { // Restore button text on close
    $(this).parent().find('.expand-section .expand span.expand-collapse').hide();
    $(this).parent().find('.expand-section .expand span.expand-default').show();
    $(this).parents('.card-content').addClass('opened').removeClass('card-expanded');
    

  });

  
})(jQuery)


</script>
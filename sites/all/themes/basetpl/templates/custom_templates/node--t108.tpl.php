
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>
  <?php
  if(!empty($content['body'])) { ?>
    <section id="main-body" class="section">
      <div class="row">
        <div class="container">
          <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <div class="inner">
            <?php print render($content['body']['#items'][0]['value']); ?>
            </div>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>

  <?php
    if(!empty($content['field_horizontal_sections'])) {
      
      $count = 0;
      $row = true;
      $field_col_obj = null;
      // echo '<pre>';
      // var_dump($content['field_bp_section']); 
      // echo '</pre>';
      foreach($content['field_horizontal_sections']['#items'] as $entity_uri) {
        $field_col_item = entity_load('field_collection_item', $entity_uri);
        
        foreach ($field_col_item as $field_col_obj) {
          $count++;
        ?>
          <section id="section-<?php print $count ?>" class="section">
            <div class="container">
              <div class="row">
                <?php if(!empty($field_col_obj->field_hs_image['und'])) { ?>
                <div class="col-xs-12 col-sm-3 col-sm-offset-1 section-img">
                  <a href="<?php print render($field_col_obj->field_cta_link_horiz['und'][0]['url']); ?>">
                    <?php 
                    print theme(
                      'image_style', 
                      array(
                        'style_name'  => 'default', 
                        'path'        => $field_col_obj->field_hs_image['und'][0]['uri'], 
                        'alt'         => $field_col_obj->field_hs_image['und'][0]['field_file_image_alt_text']['und'][0]['value'],
                        'width'       => '100%'

                      )
                    ); 
                    ?>    
                  </a>
                </div>
                <?php } ?>
                <div class="col-xs-12 col-sm-7">                 
                  <?php print render($field_col_obj->field_hs_description['und'][0]['value']); ?>
                </div>
                
              </div>
            </div> <!-- container -->
          </section> <!-- /.node -->
        <?php
        } /* foreach */
      } /* foreach */
    } /* if */
  ?>
  
</article>

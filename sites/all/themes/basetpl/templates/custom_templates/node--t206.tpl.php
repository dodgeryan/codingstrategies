<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<section class="section">
  
    <div class="row">
      <?php if (!empty($node->field_image['und'])) { ?>
       
        <div class="col-xs-12 col-sm-4 col-md-3">
          <?php 
            print theme(
              'image_style', 
              array(
                'style_name'  => 'default', 
                'path'        => $node->field_image['und'][0]['uri'], 
                'alt'         => $node->field_image['und'][0]['field_file_image_alt_text']['und'][0]['value'],
                'width'       => '100%'
              )
            );
          ?>
        </div>
      <?php } ?>
      <div class="col-xs-12 col-md-8 col-sm-9">
        <?php if(!empty($content['field_t206_date']['#items'])) { ?>
          <div class="event-date">           
            <?php print render($content['field_t206_date']); ?>          
          </div>
        <?php } ?>
        <div class="main-content">
          <?php print render($content['body']['#items'][0]['value']); ?>
        </div>
        
        <?php if(!empty($content['field_cta_link']['#items'])) { ?>
          <div class="event-link">
            <a class="btn btn-link" href="<?php print render($content['field_cta_link']['#items'][0]['uri']); ?>"><span class="fa fa-link"></span> <?php print render($content['field_cta_link']['#items'][0]['title']); ?></a>         
          </div>
        <?php } ?>
          
      </div>
    </div>
 
</section>

</article> <!-- /.node -->

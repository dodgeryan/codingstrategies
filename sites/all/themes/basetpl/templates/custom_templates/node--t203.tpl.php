<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>

<section class="section">
  
    <div class="row">
      <?php 
        if(!empty($content['field_image']['#items'])) { ?>
        <div class="col-xs-12 col-sm-4 col-md-3">
          <?php 
            print theme(
              'image_style', 
              array(
                'style_name'  => 'default', 
                'path'        => $node->field_image['und'][0]['uri'], 
                'alt'         => $node->field_image['und'][0]['field_file_image_alt_text']['und'][0]['value'],
              )
            );
          ?>
        </div>
      <?php } ?>
      

      <div class="col-xs-12 col-md-8 col-sm-9">
        <div class="main-content">
          <?php print render($content['body']['#items'][0]['value']); ?>
        </div>
        <div class="article-links row">
          <?php 
            if(!empty($content['field_media_pdf']['#items'])) {
            $pdfuri = $content['field_media_pdf']['#items'][0]['uri']; 
            $pdfile = file_create_url($pdfuri); 
          ?>
          <div class="pdf-dl">           
            <a class="btn btn-dl" href="<?php print $pdfile; ?>"><span class="fa fa-file"></span> Download PDF</a>            
          </div>
          <?php } ?>
          <?php if(!empty($content['field_article_link']['#items'])) { ?>
            <div class="article-link">
              <a class="btn btn-link" href="<?php print render($content['field_article_link']['#items'][0]['url']); ?>"><span class="fa fa-link"></span> <?php print render($content['field_article_link']['#items'][0]['title']); ?></a>         
            </div>
          <?php } ?>
        </div>
          
      </div>
    </div>
 
</section>

</article> <!-- /.node -->

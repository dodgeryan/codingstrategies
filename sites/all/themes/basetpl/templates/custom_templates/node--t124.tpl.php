<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <section id="content-cards" class="section">
    <div class="container">
      <div class="row">
        <style>
        .icon-image {width: 40px; height: auto;}
        </style>
        <?php 
          $cols = 0;
          if ((!empty($content['field_card_column_1'])) && (!empty($content['field_card_column_2'])) && (!empty($content['field_card_column_3']))) {
            $cols = 3;
          } if ((!empty($content['field_card_column_1'])) && (!empty($content['field_card_column_2'])) && (empty($content['field_card_column_3']))) {
            $cols = 2;
          } if ((!empty($content['field_card_column_1'])) && (empty($content['field_card_column_2'])) && (!empty($content['field_card_column_3']))) {
            $cols = 1;
          } 
        ?>
          <!-- UNCOMMENT IF USING WISTIA <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script>-->
          <?php // FIRST COLUMN RENDER 
            if(!empty($content['field_card_column_1']['#items'])) {
              $col1count=0;
              
              switch ($cols) {
                case '1':
                  print '<div class="col-sm-12">';
                  break;
                case '2':
                  print '<div class="col-sm-12 col-md-6">';
                  break;
                default:
                  print '<div class="col-sm-12 col-md-4">';
                  break;
              }          
              foreach($content['field_card_column_1']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col1count++;
                  $accent = $field_col_obj->field_style_select['und'][0]['value'];

                  ?>
                  <div class="row <?php print render($field_col_obj->field_style_select['und'][0]['value']); ?>">
                    <div class="card-content row">
                      <?php if ($accent === 'accentcard') { ?>
                        <?php if (!empty($field_col_obj->field_icon['und'])) {
                          print '<div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0 accent-icon">';
                          
                            print theme(
                              'image_style', 
                              array(
                                'style_name'  => 'default', 
                                'path'        => $field_col_obj->field_icon['und'][0]['uri'], 
                                'attributes'  => array(
                                  'class'       => 'icon-image',
                                  'alt'         => $field_col_obj->field_icon['und'][0]['alt'],
                                  'width'       => '40'
                                )
                              )
                            ); 
                          
                          print '</div>';
                         
                          print '<div class="col-xs-12 col-sm-8 accent-intro">';
                        } else {
                          print '<div class="col-xs-12 accent-intro">';
                        }  
                        if(!empty($field_col_obj->field_web_process_intro['und'])) {
                          print render($field_col_obj->field_web_process_intro['und'][0]['summary']); 
                        } ?>
                        </div>
                      <?php } 
                      else { ?>

                        <div class="card-header row">
                          <?php  
                            // CARD TITLE
                            if (!empty($field_col_obj->field_section_title['und'])) {
                              print '<div class="card-title col-xs-10"><h4>'.render($field_col_obj->field_section_title['und'][0]['value']).'</h4></div>'; 
                            } 
                            // CARD ICON 
                            if (!empty($field_col_obj->field_icon['und'])) {
                              print '<div class="card-icon col-xs-2">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_icon['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'icon-image',
                                    'alt'       => $field_col_obj->field_icon['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            }
                          ?>
                        </div>
                        <!--  INTRO SECTION -->
                        <div class="card-intro row">
                          <?php
                            if(!empty($field_col_obj->field_web_process_intro['und'])) {
                              print render($field_col_obj->field_web_process_intro['und'][0]['summary']); 
                            } 
                          ?>
                          
                          <!-- COLLAPSED SECTION -->
                          <div id="#collapse-<?php print $col1count; ?>" class="collapse fade intro-expanded">
                            <?php 
                            if(!empty($field_col_obj->field_web_process_intro['und'])) {
                              print render($field_col_obj->field_web_process_intro['und'][0]['value']); 
                            } 
                            ?>
                            <!-- CTA LINK -->
                            <?php if(!empty($field_col_obj->field_cta_link['und'])) { ?>
                            <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>" class="cta-link btn" target="_blank"><?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?></a>
                            <?php } ?>
                            <!-- PARDOT FORM -->
                            <?php if(!empty($field_col_obj->field_pardot_form['und'])){ ?>
                              <div class="panel-group cta-button-dropdown" id="accordion">
                                <div class="panel panel-default">
                                  <button class="panel-heading  btn-block btn-collapse" style="white-space:normal;" data-toggle="collapse" data-parent="#accordion" href="#col1<?php print $col1count; ?>"><?php print render($field_col_obj->field_pardot_form['und'][0]['summary']); ?></button>
                                  <div id="col1<?php print $col1count; ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                      <?php print render($field_col_obj->field_pardot_form['und'][0]['value']); ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                            <!-- VIDEO -->
                            <?php if(!empty($field_col_obj->field_video_yt['und'])) { ?>
                              <div class="card-video">
                                <?php $videoTitle = render($field_col_obj->field_video_yt['und'][0]['filename']);
                                  print '<p><strong>'.$videoTitle.'</strong></p>';
                                 ?>
                                 <!-- YOUTUBE EMBED REMOVE IF NOT USING -->
                                 <div class="embed-container">
                                  <iframe src="http://www.youtube.com/embed/<?php print substr($field_col_obj->field_video_yt['und'][0]['uri'], 13); ?>" frameborder="0" allowfullscreen></iframe>
                                </div>
                                <!-- UNCOMMENT FOR WISTIA  <div id="wistia_<?php // print substr($field_col_obj->field_video_yt['und'][0]['uri'], 11); ?>" class="wistia_embed">&nbsp;</div> -->
                                <?php 
                                  // UNCOMMENT FOR WISTIA ---->>>
                                  // $videothumb = substr($field_col_obj->field_video_yt['und'][0]['uri'], 11);
                                  
                                  // print '<script type="text/javascript">
                                  //   (function($) {
                                  //     var videoitem = '.json_encode($videothumb).';
                                  //     console.log(videoitem);
                                      
                                  //       wistiaEmbed = Wistia.embed(videoitem, {
                                  //         version: "v1",
                                  //         playerColor: "a9ae37",
                                  //         videoFoam: true,
                                  //         videoQuality: "auto"
                                  //       });
                                  //   })(jQuery)
                                  // </script>';
                                ?>
                               
                              </div>
                            <?php } ?>
                            
                          </div> <!-- //. COLLAPSE SECTION -->
                          <!-- EXPAND BUTTON -->
                          <div class="expand-link">
                            <a class="expand" data-toggle="collapse"  data-target="#collapse-<?php print $col1count; ?>" href="#collapse-<?php print $col1count; ?>"><span class="fa fa-plus"></span></a>
                          </div>
                        </div> <!-- //. INTRO -->
                      <?php } ?>
                    </div> <!-- //. CARD CONTENT -->
                  </div> <!-- //. CARD CELL -->

                <?php
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            } // .endif 
          ?>
          <?php // SECOND COLUMN RENDER 
            if(!empty($content['field_card_column_2']['#items'])) {
              $col2count=0;
              switch ($cols) {
                case '1':
                  print '<div class="col-sm-12">';
                  break;
                case '2':
                  print '<div class="col-sm-12 col-md-6">';
                  break;
                default:
                  print '<div class="col-sm-12 col-md-4">';
                  break;
              }           
              foreach($content['field_card_column_2']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col2count++;
                  $accent = $field_col_obj->field_style_select['und'][0]['value'];

                  ?>
                  <div class="row <?php print render($field_col_obj->field_style_select['und'][0]['value']); ?>">
                    <div class="card-content row">
                      <?php if ($accent === 'accentcard') { ?>
                        <?php if (!empty($field_col_obj->field_icon['und'])) {
                          print '<div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0 accent-icon">';
                          
                            print theme(
                              'image_style', 
                              array(
                                'style_name'  => 'default', 
                                'path'        => $field_col_obj->field_icon['und'][0]['uri'], 
                                'attributes'  => array(
                                  'class'       => 'icon-image',
                                  'alt'       => $field_col_obj->field_icon['und'][0]['alt'],
                                  'width'       => '40'
                                )
                              )
                            ); 
                          
                          print '</div>';
                         
                          print '<div class="col-xs-12 col-sm-8 accent-intro">';
                        } else {
                          print '<div class="col-xs-12 accent-intro">';
                        }  
                        if(!empty($field_col_obj->field_web_process_intro['und'])) {
                          print render($field_col_obj->field_web_process_intro['und'][0]['summary']); 
                        } ?>
                        </div>
                      <?php } 
                      else { ?>

                        <div class="card-header row">
                          <?php  
                            // CARD TITLE
                            if (!empty($field_col_obj->field_section_title['und'])) {
                              print '<div class="card-title col-xs-10"><h4>'.render($field_col_obj->field_section_title['und'][0]['value']).'</h4></div>'; 
                            } 
                            // CARD ICON 
                            if (!empty($field_col_obj->field_icon['und'])) {
                              print '<div class="card-icon col-xs-2">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_icon['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'icon-image',
                                    'alt'       => $field_col_obj->field_icon['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            }
                          ?>
                        </div>
                        <!--  INTRO SECTION -->
                        <div class="card-intro row">
                          <?php
                            if(!empty($field_col_obj->field_web_process_intro['und'])) {
                              print render($field_col_obj->field_web_process_intro['und'][0]['summary']); 
                            } 
                          ?>
                          
                          <!-- COLLAPSED SECTION -->
                          <div id="#collapse-<?php print $col2count; ?>" class="collapse fade intro-expanded">
                            <?php 
                            if(!empty($field_col_obj->field_web_process_intro['und'])) {
                              print render($field_col_obj->field_web_process_intro['und'][0]['value']); 
                            } 
                            ?>
                            <!-- CTA LINK -->
                            <?php if(!empty($field_col_obj->field_cta_link['und'])) { ?>
                            <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>" class="cta-link btn" target="_blank"><?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?></a>
                            <?php } ?>
                            <!-- PARDOT FORM -->
                            <?php if(!empty($field_col_obj->field_pardot_form['und'])){ ?>
                              <div class="panel-group cta-button-dropdown" id="accordion">
                                <div class="panel panel-default">
                                  <button class="panel-heading btn-block btn-collapse" style="white-space:normal;" data-toggle="collapse" data-parent="#accordion" href="#col2Form<?php print $col2count; ?>"><?php print render($field_col_obj->field_pardot_form['und'][0]['summary']); ?></button>
                                  <div id="col2Form<?php print $col2count; ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                      <?php print render($field_col_obj->field_pardot_form['und'][0]['value']); ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                            <!-- VIDEO -->
                            <?php if(!empty($field_col_obj->field_video_yt['und'])) { ?>
                              <div class="card-video">
                                <?php $videoTitle = render($field_col_obj->field_video_yt['und'][0]['filename']);
                                  print '<p><strong>'.$videoTitle.'</strong></p>';
                                 ?>
                                 <!-- YOUTUBE EMBED REMOVE IF NOT USING -->
                                 <div class='embed-container'>
                                  <iframe src='http://www.youtube.com/embed/<?php print substr($field_col_obj->field_video_yt['und'][0]['uri'], 13); ?>' frameborder='0' allowfullscreen></iframe>
                                </div>
                                <!-- UNCOMMENT FOR WISTIA  <div id="wistia_<?php // print substr($field_col_obj->field_video_yt['und'][0]['uri'], 11); ?>" class="wistia_embed">&nbsp;</div> -->
                                <?php 
                                  // UNCOMMENT FOR WISTIA ---->>>
                                  // $videothumb = substr($field_col_obj->field_video_yt['und'][0]['uri'], 11);
                                  
                                  // print '<script type="text/javascript">
                                  //   (function($) {
                                  //     var videoitem = '.json_encode($videothumb).';
                                  //     console.log(videoitem);
                                      
                                  //       wistiaEmbed = Wistia.embed(videoitem, {
                                  //         version: "v1",
                                  //         playerColor: "a9ae37",
                                  //         videoFoam: true,
                                  //         videoQuality: "auto"
                                  //       });
                                  //   })(jQuery)
                                  // </script>';
                                ?>
                               
                              </div>
                            <?php } ?>
                            
                          </div> <!-- //. COLLAPSE SECTION -->
                          <!-- EXPAND BUTTON -->
                          <div class="expand-link">
                            <a type="button" class="expand" data-toggle="collapse"  data-target="#collapse-<?php print $col2count; ?>" href="#collapse-<?php print $col1count; ?>"><span class="fa fa-plus"></span></a>
                          </div>
                        </div> <!-- //. INTRO -->
                      <?php } ?>
                    </div> <!-- //. CARD CONTENT -->
                  </div> <!-- //. CARD CELL -->

                <?php
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            } // .endif 
          ?>

          <?php // THIRD COLUMN RENDER 
            if(!empty($content['field_card_column_3']['#items'])) {
              $col3count=0;
              switch ($cols) {
                case '1':
                  print '<div class="col-sm-12">';
                  break;
                case '2':
                  print '<div class="col-sm-12 col-md-6">';
                  break;
                default:
                  print '<div class="col-sm-12 col-md-4">';
                  break;
              }       
              foreach($content['field_card_column_3']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);

                foreach ($field_col_item as $field_col_obj) {
                  $col3count++;
                  $accent = $field_col_obj->field_style_select['und'][0]['value'];

                  ?>
                  <div class="row <?php print render($field_col_obj->field_style_select['und'][0]['value']); ?>">
                    <div class="card-content row">
                      <?php if ($accent === 'accentcard') { ?>
                        <?php if (!empty($field_col_obj->field_icon['und'])) {
                          print '<div class="col-xs-8 col-xs-offset-2 col-sm-4 col-sm-offset-0 accent-icon">';
                          
                            print theme(
                              'image_style', 
                              array(
                                'style_name'  => 'default', 
                                'path'        => $field_col_obj->field_icon['und'][0]['uri'], 
                                'attributes'  => array(
                                  'class'       => 'icon-image',
                                  'alt'       => $field_col_obj->field_icon['und'][0]['alt'],
                                  'width'       => '100%'
                                )
                              )
                            ); 
                          
                          print '</div>';
                         
                          print '<div class="col-xs-12 col-sm-8 accent-intro">';
                        } else {
                          print '<div class="col-xs-12 accent-intro">';
                        }  
                        if(!empty($field_col_obj->field_web_process_intro['und'])) {
                          print render($field_col_obj->field_web_process_intro['und'][0]['summary']); 
                        } ?>
                        </div>
                      <?php } 
                      else { ?>

                        <div class="card-header row">
                          <?php  
                            // CARD TITLE
                            if (!empty($field_col_obj->field_section_title['und'])) {
                              print '<div class="card-title col-xs-10"><h4>'.render($field_col_obj->field_section_title['und'][0]['value']).'</h4></div>'; 
                            } 
                            // CARD ICON 
                            if (!empty($field_col_obj->field_icon['und'])) {
                              print '<div class="card-icon col-xs-2">';
                              print theme(
                                'image_style', 
                                array(
                                  'style_name'  => 'default', 
                                  'path'        => $field_col_obj->field_icon['und'][0]['uri'], 
                                  'attributes'  => array(
                                    'class'       => 'icon-image',
                                    'alt'       => $field_col_obj->field_icon['und'][0]['alt']
                                  )
                                )
                              ); 
                              print '</div>';
                            }
                          ?>
                        </div>
                        <!--  INTRO SECTION -->
                        <div class="card-intro row">
                          <?php
                            if(!empty($field_col_obj->field_web_process_intro['und'])) {
                              print render($field_col_obj->field_web_process_intro['und'][0]['summary']); 
                            } 
                          ?>
                          
                          <!-- COLLAPSED SECTION -->
                          <div id="#collapse-<?php print $col3count; ?>" class="collapse fade intro-expanded">
                            <?php 
                            if(!empty($field_col_obj->field_web_process_intro['und'])) {
                              print render($field_col_obj->field_web_process_intro['und'][0]['value']); 
                            } 
                            ?>
                            <!-- CTA LINK -->
                            <?php if(!empty($field_col_obj->field_cta_link['und'])) { ?>
                            <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>" class="cta-link btn" target="_blank"><?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?></a>
                            <?php } ?>
                            <!-- PARDOT FORM -->
                            <?php if(!empty($field_col_obj->field_pardot_form['und'])){ ?>
                              <div class="panel-group cta-button-dropdown" id="accordion">
                                <div class="panel panel-default">
                                  <button class="panel-heading" style="white-space:normal;" data-toggle="collapse" data-parent="#accordion" href="#col3Form<?php print $col3count; ?>"><?php print render($field_col_obj->field_pardot_form['und'][0]['summary']); ?></button>
                                  <div id="col3Form<?php print $col3count; ?>" class="panel-collapse collapse">
                                    <div class="panel-body">
                                      <?php print render($field_col_obj->field_pardot_form['und'][0]['value']); ?>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            <?php } ?>
                            <!-- VIDEO -->
                            <?php if(!empty($field_col_obj->field_video_yt['und'])) { ?>
                              <div class="card-video">
                                <?php $videoTitle = render($field_col_obj->field_video_yt['und'][0]['filename']);
                                  print '<p><strong>'.$videoTitle.'</strong></p>';
                                 ?>
                                 <!-- YOUTUBE EMBED REMOVE IF NOT USING -->
                                 <div class='embed-container'>
                                  <iframe src='http://www.youtube.com/embed/<?php print substr($field_col_obj->field_video_yt['und'][0]['uri'], 13); ?>' frameborder='0' allowfullscreen></iframe>
                                </div>
                                <!-- UNCOMMENT FOR WISTIA  <div id="wistia_<?php // print substr($field_col_obj->field_video_yt['und'][0]['uri'], 11); ?>" class="wistia_embed">&nbsp;</div> -->
                                <?php 
                                  // UNCOMMENT FOR WISTIA ---->>>
                                  // $videothumb = substr($field_col_obj->field_video_yt['und'][0]['uri'], 11);
                                  
                                  // print '<script type="text/javascript">
                                  //   (function($) {
                                  //     var videoitem = '.json_encode($videothumb).';
                                  //     console.log(videoitem);
                                      
                                  //       wistiaEmbed = Wistia.embed(videoitem, {
                                  //         version: "v1",
                                  //         playerColor: "a9ae37",
                                  //         videoFoam: true,
                                  //         videoQuality: "auto"
                                  //       });
                                  //   })(jQuery)
                                  // </script>';
                                ?>
                               
                              </div>
                            <?php } ?>
                            
                          </div> <!-- //. COLLAPSE SECTION -->
                          <!-- EXPAND BUTTON -->
                          <div class="expand-link">
                            <a type="button" class="expand" data-toggle="collapse"  data-target="#collapse-<?php print $col3count; ?>" href="#collapse-<?php print $col3count; ?>"><span class="fa fa-plus"></span></a>
                          </div>
                        </div> <!-- //. INTRO -->
                      <?php } ?>
                    </div> <!-- //. CARD CONTENT -->
                  </div> <!-- //. CARD CELL -->

                <?php
                } // .foreach
              } // .foreach
            print '</div>'; //. col
            } // .endif 
          ?>        

        
      </div> <!-- /. row -->
    </div> <!-- /. container -->
  </section>

</article> <!-- /.node -->

<script type="text/javascript">
(function($){                           

  var $myIntro = $(this).find('.intro-expanded');
  var $card = $('.card-content');


   // Collapse event trigger
  $card.on('click', function() {
    $card.not($(this)).find('.collapse.in').collapse('hide'); // close all other cards
    $(this).find('.intro-expanded').collapse('show'); // fire event
    $card.unbind('click', '.intro-expanded');
   
  });
  $('.expand').on('click', function(e){
    $(this).parents('.card-intro').find('.intro-expanded').collapse('toggle'); // fire event
    $(this).removeClass('expanded');
    e.preventDefault();
  })
$('.intro-expanded').on('show.bs.collapse', function () { // Change button text on reveal
    $(this).parent().find('.expand').html('<span class="fa fa-minus"></span>');
    $("html, body").animate({ scrollTop: $(this).parent().find('.expand').offset().top -375 }, 600);
    $(this).parents('.card-content').addClass('card-expanded').removeClass('opened');

    
  });

  $('.intro-expanded').on('hide.bs.collapse', function () { // Restore button text on close
    $(this).parent().find('.expand').html('<span class="fa fa-plus"></span>');
    $(this).parents('.card-content').addClass('opened').removeClass('card-expanded');
    
  });

  
})(jQuery)


</script>

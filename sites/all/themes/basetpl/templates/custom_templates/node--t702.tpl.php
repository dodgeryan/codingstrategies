<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  
  
    <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>
  
  
  <?php if (!empty($content['body']['#items'])) { ?>
    <section id="intro" class="section intro">
      <div class="container">
        <div class="row">
          
          <div class="col-xs-12 col-sm-10 col-sm-offset-1">
            <?php print render($content['body']['#items'][0]['value']); ?>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>
  
  <?php if(!empty($content['field_t702_page_section']['#items'])) {
    
    $count = 0;
    $row = true;
    
    foreach($content['field_t702_page_section']['#items'] as $entity_uri) {
      $field_col_item1 = entity_load('field_collection_item', $entity_uri);
      

      foreach ($field_col_item1 as $field_col_obj1) {
        $count++;  ?>
        <section id="section-<?php print $count ?>" class="section <?php print (($row = !$row)? 'odd' : 'even'); ?>">
          <div class="container">
            <div class="row">
              <?php if(!empty($field_col_obj1->field_section_summary_img)) {
              if(!empty($field_col_obj1->field_section_summary_img['und'][0]['field_file_image_alt_text']['und'])) {
                $imgalt = $field_col_obj1->field_section_summary_img['und'][0]['field_file_image_alt_text']['und'][0]['value'];
              } else {
                $imgalt = '';
              } ?>
                <div class="col-xs-12 col-sm-4 section-img <?php print (($zebra = !$zebra)? '' : ''); ?>">
                  
                  <?php 
                  print theme(
                    'image_style', 
                    array(
                      'style_name'  => 'default', 
                      'path'        => $field_col_obj1->field_section_summary_img['und'][0]['uri'], 
                      'alt'         => $imgalt,
                      'width'       => '100%'

                    )
                  ); 
                  ?>    
                </div>
              <?php } ?>
              <div class="col-xs-12 col-sm-8">
                <h2>
                  <?php print render($field_col_obj1->field_section_title['und'][0]['value']);
                
                  if(!empty($field_col_obj1->field_section_sub_title['und'])) { 
                    echo '<br /><em class="sub-heading">';
                    print render($field_col_obj1->field_section_sub_title['und'][0]['value']);
                    echo "</em>";
                  } ?>
                </h2>
                <?php if(!empty($field_col_obj1->field_section_summary_text['und'])) { 
                  print render($field_col_obj1->field_section_summary_text['und'][0]['value']); 
                } ?>
                <?php if(!empty($field_col_obj1->field_section_content['und'])) { ?>
                  <a href="#section-detail-<?php print $count ?>" class="btn section-link" data-toggle="collapse" data-target="#section-detail-<?php print $count ?>"><?php print render($field_col_obj1->field_cta_line['und'][0]['value']); ?></a>
                <?php } ?>
              </div>
              
            </div>
          </div> <!-- container -->
          <!-- SECOND LEVEL -->
          <div id="section-detail-<?php print $count ?>" class="section-detail collapse">
            <div class="container">
              <?php if(!empty($field_col_obj1->field_section_content['und'])) { ?>              
                <div class="row section">
                  <div class="col-xs-12 col-sm-10 col-sm-offset-1">
                    <?php print render($field_col_obj1->field_section_content['und'][0]['value']); ?>
                  </div>
                </div>
              <?php } ?>
              <div class="row">
                <?php
                  $secondLevel = 0;
                  if(!empty($field_col_obj1->field_t703_objectives['und'])) {
                    foreach ($field_col_obj1->field_t703_objectives['und'] as $entity_uri2) {
                      $field_col_item2 = entity_load('field_collection_item', $entity_uri2);
                      
                      foreach ($field_col_item2 as $field_col_obj2) {
                        $secondLevel++; ?>

                        
                        <div class="col-xs-12 col-sm-6">
                          <div class="col-xs-12 col-sm-2">
                            <?php 
                              if(!empty($field_col_obj2->field_t703_section_img['und'])) {
                                if(!empty($field_col_obj2->field_t703_section_img['und'][0]['field_file_image_alt_text']['und'])) {
                                  $imgalt2 = $field_col_obj2->field_t703_section_img['und'][0]['field_file_image_alt_text']['und'][0]['value'];
                                } else {
                                  $imgalt2 = '';
                                }
                                print theme(
                                  'image_style', 
                                  array(
                                    'style_name'  => 'default', 
                                    'path'        => $field_col_obj2->field_t703_section_img['und'][0]['uri'], 
                                    'alt'         => $imgalt2,
                                    'width'       => '100%'

                                  )
                                ); 
                              }
                            ?> 
                          </div> 
                          <div class="col-xs-12 col-sm-9">
                            <?php print render($field_col_obj2->field_t703_section_txt['und'][0]['value']); ?>
                          </div>
                        </div>
                      

                      <?php } /* foreach 2 */ ?>
                    <?php } /* foreach 1 */ ?>
                <?php } /* if not empty */ ?>              
              </div>
            </div>
          </div>
        </section> <!-- /.node -->
      <?php } /* foreach */
    } /* foreach */
  } /* if */
  ?>
<!-- CTA SECTION -->
<?php if(!empty($content['field_cta']['#items']) || !empty($content['field_cta_link'])) { ?>
  <section class="page-cta section">    
    <div class="container">
      <div class="row">
        <?php if(!empty($content['field_cta']['#items'])) { ?>
          <div class="cta-text col-xs-12 col-sm-8 col-sm-offset-2">          
            <?php print render($content['field_cta']['#items'][0]['value']); ?>
          </div>  
        <?php } ?> 
        <?php if(!empty($content['field_cta_link'])) { ?>               
          <div class="col-xs-12 cta-link">
            <?php 
              print '<a class="cta-btn btn btn-default" href="';
                print render($content['field_cta_link']['#items'][0]['url']);
              print '">';
                print render($content['field_cta_link']['#items'][0]['title']);
              print '</a>';
            ?>         
          </div>
        <?php } ?>
      </div>
    </div>
   
  </section>
<?php } ?>
  <!-- END  CTA -->  
  
</article>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <?php if(!empty($content['body']['#items'])) { ?>
    <section id="page-text">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <?php print render($content['body']['#items'][0]['value']); ?>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>


<?php if(!empty($content['field_faqs']['#items'])) {
       $count = 0;
  ?>
  <section id="accordion">
    <div class="container">
      <div class="row">
        <div class="panel col-md-10 col-md-offset-1">
          <div class="panel-body">
            <div class="panel-group" id="faq">
              <?php foreach($content['field_faqs']['#items'] as $entity_uri) {
                $field_col_item = entity_load('field_collection_item', $entity_uri);
                foreach ($field_col_item as $field_col_obj) { 
                  $count++; ?>
                  <div class="panel panel-default">
                    <a data-toggle="collapse" data-parent="#faq" href="#faq<?php print $count; ?>" class="faq-more collapsed">
                      <div class="panel-heading">
                        <div class="faq-question">
                          <h4><?php print render($field_col_obj->field_faq_question['und'][0]['value']); ?></h4>
                        </div>
                      </div>
                    </a>
                    
                    <div id="faq<?php print $count; ?>" class="panel-collapse collapse">
                      <div class="panel-body">
                        <div class="faq-text">
                            <?php print render($field_col_obj->field_answer['und'][0]['value']); ?>
                        </div>
                      </div>
                    </div>
                  </div>                  
                  <?php 
                } /* foreach */
              } /* foreach */ ?>
            </div><!-- #faq -->
          </div><!-- .panel-body -->
        </div> <!-- .panel -->
      </div><!-- .row -->
    </div><!-- container -->
  </section> 
<?php  } /* if */ ?>
</article><!-- /.node -->

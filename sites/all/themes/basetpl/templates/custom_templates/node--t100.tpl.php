<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?>"<?php print $attributes; ?>>
  <section id="hero" class="jumbotron">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-lg-8 col-lg-offset-2">
          <h1 id="home-title"><?php print $title; ?></h1>
        </div>

        <div class="col-sm-12 col-md-10 col-md-offset-1 col-lg-8 col-lg-offset-2 hidden-xs" id="hero-spotlights">
          <?php if(!empty($content['field_hero_spotlights'])) {
            foreach($content['field_hero_spotlights']['#items'] as $entity_uri) {
              $field_col_item = entity_load('field_collection_item', $entity_uri);

              foreach ($field_col_item as $field_col_obj) { ?>
                <div class="col-xs-12 col-sm-4">
                  
                  <div class="wow fadeInDown">
                    <!-- front content -->
                    <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>" class="btn-cta btn-mkt">
                      
                      <div class="img">
                        <?php
                          print theme(
                            'image_style', 
                            array(
                              'style_name'  => 'default', 
                              'path'        => $field_col_obj->field_hero_icon['und'][0]['uri'], 
                              'attributes'  => array(
                                'alt'       => $field_col_obj->field_hero_icon['und'][0]['alt'],
                                'width'     => '100%'
                              )
                            )
                          ); 
                        ?>
                      </div>

                      <div class="info">
                          <h3 class="spotlight-title"><?php print render($field_col_obj->field_spotlight_title['und'][0]['value']); ?></h3>
                          <div class="cta-link">
                              <?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?>
                          </div>
                      </div>

                    </a>

                  </div>

                </div> 
                <?php 
              }
            }  
          }
          ?>
        </div>

      </div>
    </div>

      <div class="section">
        <div class="container">
          <div class="row">
            <div class="col-xs-12 col-lg-8 col-lg-offset-2 hero-text">
              <div class="lead"><?php print render($content['body']['#items'][0]['value']); ?></div>
            </div>
          </div>
        </div>       
      </div>

      <div class="col-xs-12 visible-xs" id="hero-spotlights">
          <?php if(!empty($content['field_hero_spotlights'])) {
            foreach($content['field_hero_spotlights']['#items'] as $entity_uri) {
              $field_col_item = entity_load('field_collection_item', $entity_uri);

              foreach ($field_col_item as $field_col_obj) { ?>
                <div class="col-xs-12" id="hero-spotlight-item">
                  
                  <div class="wow fadeInDown">
                    <!-- front content -->
                    <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>" class="btn-cta btn-mkt">
                      
                      <div class="col-xs-4 img">
                        <?php
                          print theme(
                            'image_style', 
                            array(
                              'style_name'  => 'default', 
                              'path'        => $field_col_obj->field_hero_icon['und'][0]['uri'], 
                              'attributes'  => array(
                                'alt'       => $field_col_obj->field_hero_icon['und'][0]['alt'],
                                'width'     => '100%'
                              )
                            )
                          ); 
                        ?>
                      </div>

                      <div class="col-xs-8 info">
                          <h3 class="spotlight-title"><?php print render($field_col_obj->field_spotlight_title['und'][0]['value']); ?></h3>
                          <div class="cta-link">
                              <?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?>
                          </div>
                      </div>

                    </a>

                  </div>

                </div>
                <div class="col-xs-12">
                  <hr>
                </div>
                <?php 
              }
            }  
          }
          ?>
        </div>

  </section>

  <section class="spotlight">
    <div class="container">
      <div class="row">
        <?php if(!empty($content['field_spotlights'])) {
          foreach($content['field_spotlights']['#items'] as $entity_uri) {
            $field_col_item = entity_load('field_collection_item', $entity_uri);

            foreach ($field_col_item as $field_col_obj) { ?>
              <div class="col-xs-12 col-sm-6">
                <h3><?php print render($field_col_obj->field_section_title['und'][0]['value']); ?></h3>
                <div class="spotlight-text"><?php print render($field_col_obj->field_cs_section_txt['und'][0]['value']); ?></div>
                <div class="cta-link">
                  <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>"><?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?> <i class="fa fa-chevron-circle-right"></i></a>
                </div>
              </div>
            <?php }
          }
        } ?>
      </div>
    </div>
  </section>
  <!-- 
  <section class="spotlight section">
    <div class="container marketing">
      <div class="row">
        <?php
            // $view = views_get_view('2_up');
            // $view->set_display('block');
            // $output = $view->preview();
            // // now print the view.
            // if ($view->result) {
            //   print views_embed_view('2_up', $display_id = 'block');
            // }
          ?>
      </div>
    </div>
  </section> 
  -->
<!-- CONTENT FEEDS -->
  <section class="contentfeeds content-block section">
    <div class="container">     
      <div class="row">
        <div class="col-md-4 col-content content-item wow fadeInUp column__1" data-wow-delay="700ms">
          <p class="text-center"><span class="top-feature-left"></span><i class="fa fa-bullhorn" id="featured-icon"></i><span class="top-feature-right"></span></p>
          <h2><a href="/news">News</a></h2>
          <?php
            $view = views_get_view('news_feed');
              $view->set_display('block_3');
              $output = $view->preview();
              // now print the view.
              if ($view->result) {
                print views_embed_view('news_feed', $display_id = 'block_3');
              }
          ?>
        </div>

        <div class="clearfix visible-sm visible-xs"></div>

        <div class="col-md-4 col-content content-item wow fadeInUp column__2" data-wow-delay="700ms">
          <p class="text-center"><span class="top-feature-left"></span><i class="fa fa-calendar-o" id="featured-icon"></i><span class="top-feature-right"></span></p>
          <h2><a href="/events">Upcoming Events</a></h2>
          <?php
              $view = views_get_view('events');
              $view->set_display('block_3');
              $output = $view->preview();
              // now print the view.
              if ($view->result) {
                print views_embed_view('events', $display_id = 'block_3');
              }
            ?>
        </div>

        <div class="clearfix visible-sm visible-xs"></div>

        <div class="col-md-4 col-content content-item wow fadeInUp column__3" data-wow-delay="700ms">
          <p class="text-center"><span class="top-feature-left"></span><i class="fa blog-icon" id="featured-icon"></i><span class="top-feature-right"></span></p>
          <h2><a href="/blog">Blog</a></h2>
          <?php
            $view = views_get_view('blog');
              $view->set_display('block_3');
              $output = $view->preview();
              // now print the view.
              if ($view->result) {
                print views_embed_view('blog', $display_id = 'block_3');
              }
          ?>
        </div>
      </div>

    </div>
  </section>
  <!-- END CONTENT FEEDS-->
</article>

<script src="/sites/all/themes/basetpl/js/jquery.carouFredSel-6.2.1-packed.js" type="text/javascript"></script>
<!-- <script charset="ISO-8859-1" src="//fast.wistia.com/assets/external/E-v1.js"></script> -->
<script type="text/javascript">
  (function($){     
   
    // CAROUSEL
    $('.popclick').popover();
    
    $.fn.extend({
      popoverClosable: function (options) {
        options = $.extend({}, options);
        var $popover_togglers = this;
        $popover_togglers.popover(options);
        $popover_togglers.on('click', function (e) {
            e.preventDefault();
            $popover_togglers.not(this).popover('hide');
        });
        $('html').on('click', '[data-dismiss="popover"]', function (e) {
            $popover_togglers.popover('hide');
        });
      }
    });

    $(function () {
        $('[data-toggle="popover"]').popoverClosable();
    });
    $(window).load(function() {
      // var omfg = $(window).width();

      // Using custom configuration
      $('#carousel').carouFredSel({
        width: '100%',
        height: 117,
        auto: false,
        items: {
          width: 224,
          height: 117,
          visible : {
            min         : 1,
            max         : 4
          }
        },
        scroll : {
          items: 1,
          easing: "swing"
        },      
        prev: "#prev",
        next: "#next",
        // responsive: true,
        swipe: true,
        circular: true,
        mousewheel: true,
        swipe: {
          onMouse: true,
          onTouch: true
        }
      }).parent().css({ 'margin': '0 auto'});
      
    });
    
      
      // $('.gray-line').onScreen({
      //   container: window,
      //   direction: 'vertical',
      //   doIn: function() {
      //     $('.gray-line').animate({
      //     width: "108px"
      //     }, 900, 'linear');
      //   }
      // })
  

})(jQuery);
</script>
<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>
  
  <?php if(!empty($content['body']['#items'])) { ?>
    <section id="page-text">
      <div class="container">
        <div class="row">
          <div class="col-sm-10 col-sm-offset-1">
            <?php print render($content['body']['#items'][0]['value']); ?>
          </div>
        </div>
      </div>
    </section>
  <?php } ?>


<?php if(!empty($content['field_faq_section']['#items'])) {
       $sectionCount = 0;
       $faq=0;
  ?>
  <?php foreach($content['field_faq_section']['#items'] as $entity_uri) {
    $field_col_item1 = entity_load('field_collection_item', $entity_uri);
    foreach ($field_col_item1 as $field_col_obj1) {
      $sectionCount++; ?>
      <div class="container">
        <h3 class="faq-section-title col-md-10 col-md-offset-1"><?php print render($field_col_obj1->field_section_title['und'][0]['value']); ?></h3>
      </div>
      
      <section id="accordion-<?php print $sectionCount; ?>">
        <div class="container">
          <div class="row">
            <div class="panel col-md-10 col-md-offset-1">
              <div class="panel-body">
                <div class="panel-group" id="faq<?php print $sectionCount; ?>">
                  <?php foreach ($field_col_obj1->field_faqs['und'] as $entity_uri) {
                    $field_col_item2 = entity_load('field_collection_item', $entity_uri);
                    foreach ($field_col_item2 as $field_col_obj2) { 
                      $faq++; ?>
                      <div class="panel panel-default">
                        <a data-toggle="collapse" data-parent="#faq<?php print $sectionCount; ?>" href="#faq-<?php print $faq; ?>" class="faq-more collapsed">
                          <div class="panel-heading">
                            <div class="faq-question">
                              <h4><?php print render($field_col_obj2->field_faq_question['und'][0]['value']); ?><i class="fa fa-sort-desc"></i></h4>
                            </div>
                          </div>
                        </a>
                        
                        <div id="faq-<?php print $faq; ?>" class="panel-collapse collapse">
                          <div class="panel-body">
                            <div class="faq-text">
                                <?php print render($field_col_obj2->field_answer['und'][0]['value']); ?>
                            </div>
                          </div>
                        </div>
                      </div>                  
                      <?php 
                    } /* foreach 2.2 */
                  } /* foreach 2.1 */ ?>
                </div><!-- #faq -->
              </div><!-- .panel-body -->
            </div> <!-- .panel -->
          </div><!-- .row -->
        </div><!-- container -->
      </section> 
      <?php 
    } /* foreach 2.2 */
  } /* foreach 2.1 */ ?>
<?php  } /* if */ ?>
</article><!-- /.node -->

<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>


  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>



<section id="overview" class="section">
  <div class="container">
    <div class="row">      
      <div class="col-xs-12 col-sm-10 col-sm-offset-1">
        
        <?php if(!empty($content['body']['#items'])) { print render($content['body']['#items'][0]['value']); } ?>
      </div>
    </div>
  </div>
</section>

<?php // KEY FEATURES
if(!empty($content['field_t703_tech']['#items'])) { ?>
  <section id="tech" class="section even">
    <div class="container">
      <div class="row section">
        <?php foreach($content['field_t703_tech']['#items'] as $entity_uri) {
          $field_col_item = entity_load('field_collection_item', $entity_uri);
          
          foreach ($field_col_item as $field_col_obj) { ?>
            <div class="feature-block col-xs-12 col-sm-4">
              <?php if(!empty($field_col_obj->field_t703_section_img['und'])) {
              if(!empty($field_col_obj->field_t703_section_img['und'][0]['field_file_image_alt_text']['und'])) {
                $imgalt = $field_col_obj->field_t703_section_img['und'][0]['field_file_image_alt_text']['und'][0]['value'];
              } else {
                $imgalt = '';
              }
               ?>
                <div class="col-xs-12 feature-img">
                  <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>">
                    <?php print theme(
                      'image_style', 
                      array(
                        'style_name'  => 'default', 
                        'path'        => $field_col_obj->field_t703_section_img['und'][0]['uri'], 
                        'alt'         => $imgalt,
                      )
                    ); ?>      
                  </a>
                </div>
              <?php } ?>
              <?php if(!empty($field_col_obj->field_cta_link['und'])){ ?>
                <div class="col-xs-12 feature-cta">
                  <a href="<?php print render($field_col_obj->field_cta_link['und'][0]['url']); ?>"><h3><?php print render($field_col_obj->field_cta_link['und'][0]['title']); ?></h3></a>
                </div>       
              <?php } ?>
              <?php if(!empty($field_col_obj->field_t703_section_txt['und'])){ ?>
                <div class="col-xs-12 feature-descr">
                  <?php print render($field_col_obj->field_t703_section_txt['und'][0]['value']); ?>
                </div>       
              <?php } ?>
            </div>
          <?php } 
        } ?>
      </div>
    </div>
  </section>
<?php } ?>

<?php   // Consultants
if(!empty($content['field_t703_objectives']['#items'])) { ?>
  <section id="objectives" class="section even">     
    <div class="container">
      <h3 class="section-title"><?php print render($content['field_section_title']['#items'][0]['value']); ?></h3>
      <div class="row">        
        <?php foreach($content['field_t703_objectives']['#items'] as $entity_uri) {
          $field_col_item = entity_load('field_collection_item', $entity_uri);
          
          foreach ($field_col_item as $field_col_obj) { ?>           
            <div class="media col-xs-12 col-sm-6">
              <div class="row">
                <?php if(!empty($field_col_obj->field_t703_section_img['und'])) { ?>
                <div class="col-xs-12 col-sm-3">
                  <?php print theme(
                    'image_style', 
                    array(
                      'style_name'  => 'default', 
                      'path'        => $field_col_obj->field_t703_section_img['und'][0]['uri'], 
                      'alt'         => $field_col_obj->field_t703_section_img['und'][0]['field_file_image_alt_text']['und'][0]['value'],
                      'width'       => '100%'
                    )
                  ); ?>    
                </div>
                <?php } // img ?>
                <?php if(!empty($field_col_obj->field_t703_section_txt['und'])) { ?>
                  <div class="media-body col-xs-12 col-sm-9">
                    <?php print render($field_col_obj->field_t703_section_txt['und'][0]['value']); ?>
                  </div>
                <?php } // text ?>
              </div>
            </div>
          <?php } //3
        } //2 ?>
      </div>
    </div>
  </section>
<?php } // 1 ?>
<!-- CTA SECTION -->
<?php if(!empty($content['field_cta']['#items']) || !empty($content['field_cta_link'])) { ?>
  <section class="page-cta section">    
    <div class="container">
      <div class="row">
        <?php if(!empty($content['field_cta']['#items'])) { ?>
          <div class="cta-text col-xs-12 col-sm-8 col-sm-offset-2">          
            <?php print render($content['field_cta']['#items'][0]['value']); ?>
          </div>  
        <?php } ?> 
        <?php if(!empty($content['field_cta_link'])) { ?>               
          <div class="col-xs-12 cta-link">
            <?php 
              print '<a class="cta-btn btn btn-default" href="';
                print render($content['field_cta_link']['#items'][0]['url']);
              print '">';
                print render($content['field_cta_link']['#items'][0]['title']);
              print '</a>';
            ?>         
          </div>
        <?php } ?>
      </div>
    </div>
   
  </section>
<?php } ?>
  <!-- END  CTA -->  




  
</article>
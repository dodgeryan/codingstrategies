<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>
  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>

  <section id="page-text" class="section">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-10 col-sm-offset-1">
          <?php if (!empty($content['body']['#items'])) {
            print render($content['body']['#items'][0]['value']);
          } ?>
        </div>
      </div>
    </div>
  </section>
  <section id="logos" class="section">
    <div class="container">
      <div class="row">
        <?php if(!empty($content['field_t103_logo_section']['#items'])) {
          $count=0;
          foreach ($content['field_t103_logo_section']['#items'] as $entity_uri) {
            $field_col_item = entity_load('field_collection_item', $entity_uri);      
            foreach ($field_col_item as $field_col_obj) {
              $count++; ?>
              <div class="col-xs-12 col-sm-6 col-md-4 field-collection-item">
                <a href="<?php print render($field_col_obj->field_t103_logo['und'][0]['url']);?>" target="<?php print render($field_col_obj->field_t103_logo['und'][0]['target']);?>">
                  <?php 
                  print theme(
                    'image_style', 
                    array(
                      'style_name'  => 'default', 
                      'path'        => $field_col_obj->field_t103_logo['und'][0]['uri'], 
                      'alt'         => $field_col_obj->field_t103_logo['und'][0]['alt'],
                      'width'       => '100%'
                    )
                  ) ?>
                </a>
                
                <div class="box-text">
                  <?php print render($field_col_obj->field_t103_dscr['und'][0]['value']); ?>
                </div>
              </div>
            <?php
            }
          }

        } ?>
      </div>
    </div>
  </section>
</article> <!-- /.node -->

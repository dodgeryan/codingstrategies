<article id="node-<?php print $node->nid; ?>" class="<?php print $classes; ?> clearfix"<?php print $attributes; ?>>


  <section id="hero" class="interior-hero">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 col-sm-offset-1">
          <h1><?php print $title; ?></h1>
          <?php if(!empty($content['field_hero_descr']['#items'])) { ?>
            <div class="lead"><?php print render($content['field_hero_descr']['#items'][0]['value']); ?></div>
          <?php } ?>    
        </div>
      </div>
    </div>
  </section>

  <section id="page-text" class="section">
    <div class="container">
      <div class="row">         
        <div class="col-xs-12 col-sm-10 col-sm-offset-1 page-text">
          <?php print render($content['body']); ?>
        </div>
      </div>
    </div>
  </section>

  

</article> <!-- /.node -->

<!DOCTYPE html>
  <!--[if lt IE 7]><html class="no-js ie6 oldie" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]-->
<!--[if IE 7]><html class="no-js ie7 oldie" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]-->
<!--[if IE 8]><html class="no-js ie8 oldie" lang="en" xmlns="http://www.w3.org/1999/xhtml"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="<?php print $language->language; ?>" dir="<?php print $language->dir; ?>"<?php print $rdf_namespaces;?>>
<!--<![endif]-->
<head profile="<?php print $grddl_profile; ?>">
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
  <?php print $head; ?>
  <title><?php print $head_title; ?></title>
  
  <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

  <?php print $styles; ?>
  <link href="/sites/all/themes/basetpl/css/main.css" rel="stylesheet">


<!-- HTML5 element support for IE6-8 -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="/sites/all/themes/basetpl/js/respond.min.js"></script>
<![endif]-->

<?php print $scripts; ?>  

</head>
<body class="<?php print $classes; ?>" <?php print $attributes;?>>
  <div id="contact-page">
    <!--[if lt IE 8]>
      <p class="chromeframe">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> or <a href="http://www.google.com/chromeframe/?redirect=true">activate Google Chrome Frame</a> to improve your experience.</p>
    <![endif]-->
    <div id="skip-link">
      <a href="#main-content" class="element-invisible element-focusable"><?php print t('Skip to main content'); ?></a>
    </div>
    <?php print $page_top; ?>
    <?php print $page; ?>
  </div>
   <?php print $page_bottom; ?>
  
  <script type="text/javascript" src="/sites/all/themes/basetpl/js/mainjs.js"></script>
  <script type="text/javascript" src="/sites/all/themes/basetpl/js/jquery.pageslide.js"></script>
<script type="text/javascript">
  (function($) {
      // MOBILE MENU
  var callback = function(){
    // var $wwidth = $(window).width(); 
    var $wheight = $(window).height();
    var $menuheight = $wheight;
    
      var $break1 ='750px'; 
      var $break2 ='970px'; 
      var $break3 ='1170px'; 
    $('#pageslide #slidemenu').height(($menuheight));
  };
  
  $(window).on("load", callback);
  $(window).on("resize", callback);
  
  $(".open").pageslide();
  $('.open').on('click', callback);
  $('.close-menu').on('click', function(){
    $.pageslide.close();
    
  })
  // $('<div class="visible-xs visible-sm mobile-menu-title"><a class="gm" href=".mobile-global"><span class="fa fa-chevron-down"></span>&nbsp; more &nbsp;<span class="fa fa-chevron-down"></span></a></div>').insertBefore('.mobile-global');

  // $('.gm').on('click', function (e){
  //   e.preventDefault();
  //   $('.mobile-global').collapse('toggle');
  // })  
  })(jQuery);
</script>

</body>
</html>

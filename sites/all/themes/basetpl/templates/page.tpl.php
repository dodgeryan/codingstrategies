<?php
/**
 * CUSTOM THEME FOR CODING STRATEGIES
 */
?>

<div class="navbar global-nav" role="navigation">
  <div class="container">
    <div class="hidden-xs hidden-sm pull-right">
      <?php
        print theme('links__menu_global', array(
            'links' => $menu_global,
            'attributes' => array(
            'class' => array('nav' , 'navbar-nav', 'pull-right'),
            'role' => 'navigation'
            ),
        ));
      ?>
    </div>
    <div class="search-bar col-xs-12 col-sm-4 col-md-3 pull-right"><?php print $search_box; ?></div>
  </div>
</div>
<div class="navbar navbar-inverse" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <div class="visible-xs visible-sm pull-right"><a class="btn btn-default open" href="#slidemenu">MENU</a></div>
        <?php if ($logo): ?>
        <a class="logo pull-left navbar-brand" href="<?php print $front_page; ?>" title="<?php print t('Home'); ?>">
          <img src="<?php print $logo; ?>" alt="<?php print t('Home'); ?>" />
        </a>
        <?php endif; ?>
        <!-- .btn-navbar is used as the toggle for collapsed navbar content -->
      </div>
      <div class="hidden-xs hidden-sm pull-right">
        <?php if (!empty($primary_nav)): ?>
          <?php print render($primary_nav); ?>
        <?php endif; ?>
      </div>
    </div><!--/.navbar-header -->
  </div>
</div>

<?php if(!empty($page['header'])) { ?>
  <header role="banner" id="page-header">
    <?php print render($page['header']); ?>
  </header> <!-- /#page-header -->
<?php } ?>
<?php if($is_front) {
  print '<div class="main-container">';
} else { ?>
<div class="main-container <?php if((!isset($node)) || $contain) { ?>container<?php } ?>">
  <?php } ?>
  <?php 
    if (!empty($page['highlighted'])) { 
    // $url = $_SERVER['REQUEST_URI'];
    // if (parse_url($url, PHP_URL_QUERY)) { 
    
    // } else { ?>
    <section id="hero">
      <div class="highlighted jumbotron"><?php print render($page['highlighted']); ?></div>
    </section>
   <?php }//}  ?>
  <div class="row">
    <?php if(!empty($page['content_top'])) {
      print '<div class="container"><div class="row">' .render($page['content_top']). '</div></div>';
    } ?>
    <?php if (!empty($page['sidebar_first'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_first']); ?>
      </aside>  <!-- /#sidebar-first -->
    <?php endif; ?>

    <section id="page-content"<?php print $content_column_class; ?>>
        <?php if (!empty($breadcrumb)): print $breadcrumb; endif;?>
        <a id="main-content"></a>
        <?php print render($title_prefix); ?>
        <?php if (!$is_front  && !empty($title)): ?>
          <h1 class="page-header col-sm-12"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
        <div class="container">
          <?php print $messages; ?>
          <?php if (!empty($tabs)): ?>
              <?php print render($tabs); ?>
          <?php endif; ?>
          <?php if (!empty($page['help'])): ?>
            <?php print render($page['help']); ?>
          <?php endif; ?>
          <?php if (!empty($action_links)): ?>
            <ul class="action-links"><?php print render($action_links); ?></ul>
          <?php endif; ?>
        </div>
      <?php print render($page['content']); ?>
    </section>

    <?php if (!empty($page['sidebar_second'])): ?>
      <aside class="col-sm-3" role="complementary">
        <?php print render($page['sidebar_second']); ?>
      </aside>  <!-- /#sidebar-second -->
    <?php endif; ?>

  </div>
</div>
<?php if(!empty($page['postscript'])) { ?>
  <section id="postscript" class="section">
    <div class="postscript">
      <div class="container">
        <div class="row">
          <?php print render($page['postscript']); ?>
        </div>
      </div>
    </div>
  </section>
<?php } ?>
<div class="clearfix"></div>
<?php //if(!empty($page['footer'])) { ?>
  <div id="footer" class="footer row">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-md-10">
          <?php if(!empty($page['footer'])) {
            print render($page['footer']);
          }?>
          
        </div>
        <div class="col-xs-12 col-md-2 social-block">
          <h2 class="block-title">Connect</h2>
          <a class="social social-fb" href="https://www.facebook.com/CodingStrategiesGA">Facebook</a>
          <a class="social social-in" href="https://www.linkedin.com/company/coding-strategies">LinkedIn</a>
        </div>
      </div>
      <div class="row">
        <div class="col-xs-12">
          <p class="copyright">All content and trademarked terms are the sole property of Coding Strategies, Inc. &copy; 2014</p>
        </div>
      </div>
    </div>
  </div>
<?php //} ?>
<nav id="slidemenu" role="navigation">
  <button role='button' class="close close-menu"><span class='fa fa-times'></span> </button>
  <h3 class="mobile-menu-title">Menu</h3>
  <?php if (!empty($primary_nav)): ?>
            <?php print render($primary_nav); ?>
          <?php endif; ?>

  <div class="mobile-global">

  <?php
      print theme('links__menu_global', array(
          'links' => $menu_global,
          'attributes' => array(
          'class' => array('nav' , 'navbar-nav', 'hidden-md', 'hidden-lg'),
          'role' => 'navigation'
          ),
      ));
    ?>
  </div>
    
</nav>

